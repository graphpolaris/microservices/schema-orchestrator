.PHONY: all dep build test lint

lint: dep ## Lint the files
	@golint -set_exit_status ./...

test: dep ## Run unittests
	@go test -cover -coverprofile=coverage.txt -covermode count ./...

dep: login ## Get the dependencies
	@go get -a -v ./...
	@go get -u golang.org/x/lint/golint
	@go get -u github.com/boumenot/gocover-cobertura

coverage: dep
	@go test -v -coverpkg=./... -coverprofile=cover.out ./...
	@go tool cover -func cover.out | grep total
	@go tool cover -html=cover.out -o cover.html

windows:
	$(eval export GOOS := windows)
	@go build -o builds/main ./cmd/schema-orchestrator/

macos:
	$(eval export GOOS := darwin)
	@go build -o builds/main ./cmd/schema-orchestrator/

linux: login # Build for linux
	$(eval export GOOS := linux)
	CGO_ENABLED=0 go build -o builds/main ./cmd/schema-orchestrator/

run:
	./builds/main

develop:
	# Usernames and Password only usable in locally in dev environment!
	$(eval export RABBIT_USER := rabbitmq) 
	$(eval export RABBIT_PASSWORD := DevOnlyPass) 
	$(eval export RABBIT_HOST := localhost)
	$(eval export RABBIT_PORT := 5672)
	
	$(eval export REDIS_ADDRESS := localhost:6379)
	$(eval export REDIS_PASSWORD := DevOnlyPass)

	$(eval export JWT_SECRET := 15D262E1FB339FFBD062FFB81C1831B2757FA3F1C02B7432A3E586A447FB7870)

	$(eval export LOG_MESSAGES := false)
	$(eval export LOG_LEVEL := -1)
	$(eval export DEV := true)
	$(eval export USER_MANAGEMENT_SERVICE_RPC := localhost:9000)
	$(eval export ALLOWED_ORIGINS := http://localhost:4200)

	@go run cmd/schema-orchestrator/main.go

docker: login
	@docker build -t harbor.graphpolairs.com/graphpolaris/schema-orchestrator:latest .

rollout: docker
	kubectl rollout restart deployment schema-orchestrator

log: 
	@kubectl get pods -o wide | grep Running | grep schema-orchestrator | cut -d " " -f1 | xargs kubectl logs -f

login:
	echo -e "machine git.science.uu.nl\nlogin gitlab-ci-token\npassword ${CI_JOB_TOKEN}" > ~/.netrc
	
tidy:
	GOPROXY=direct go get -u -v all 
	@go mod tidy --compat=1.21

push: tidy
	@docker build --progress plain -t harbor.graphpolairs.com/graphpolaris/schema-orchestrator:latest .