/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package main

import (
	"schema-orchestrator/internal/usecases/produce"
	"schema-orchestrator/internal/usecases/schemaService"
	"schema-orchestrator/internal/usecases/schemaStatsService"

	"git.science.uu.nl/graphpolaris/go-common/microservice"
)

/*
This is the main method, it executes the code for this service
*/
func main() {
	// Set up logging
	microservice.SetupLogging()

	// Create broker driver
	brokerDriver := microservice.ConnectRabbit()

	// Create keyvaluestore to get the queue with which clients are connected (websockets)
	redisService := microservice.ConnectRedis()

	// Create producer service
	produceService := produce.New(brokerDriver, redisService)
	produceService.Start()

	// services
	schemaService := schemaService.New(produceService, redisService)
	schemaStatsService := schemaStatsService.New(produceService, redisService)

	// Setup web driver
	api := microservice.New()
	r := api.Routes()
	r.Post("/", schemaService.Handler)
	r.Post("/stats/", schemaStatsService.Handler)
	api.Start(3002, r)

	select {}
}
