## Development

For quick development, add to the go.mod file these two replace lines, which will fetch the respective dependency code from the local repo instead of from git:

```sh
replace git.science.uu.nl/graphpolaris/broker => ../../dependencies/broker

replace git.science.uu.nl/graphpolaris/keyvaluestore => ../../dependencies/keyvaluestore

```
