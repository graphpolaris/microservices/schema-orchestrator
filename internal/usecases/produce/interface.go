/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package produce

import (
	"git.science.uu.nl/graphpolaris/broker"
	"git.science.uu.nl/graphpolaris/broker/producer"
	"git.science.uu.nl/graphpolaris/keyvaluestore"
)

/*
Service wraps consumer methods
*/
type ProduceService struct {
	brokerDriver    broker.Interface
	keyValueStore   keyvaluestore.Interface
	RequestProducer producer.BrokerProducerI
	ResultProducer  producer.BrokerProducerI
}

/*
New creates a new service

	broker: broker.Interface, the broker for the new service
	keyValueStore: keyvaluestore.Interface, the key value store for the new service
	rpcDriver: rpcdriver.Interface, the rpc driver of the new interface
	Return: *Service, the new service
*/
func New(broker broker.Interface, keyValueStore keyvaluestore.Interface) *ProduceService {
	return &ProduceService{
		brokerDriver:  broker,
		keyValueStore: keyValueStore,
	}
}

/*
Start starts the producer
*/
func (s *ProduceService) Start() {
	// Create producer
	s.RequestProducer = s.brokerDriver.CreateProducer("requests-exchange", "schema-orchestrator")
	s.ResultProducer = s.brokerDriver.CreateProducer("ui-direct-exchange", "schema-orchestrator")
}
