package schemaStatsService

import (
	"encoding/json"
	"fmt"
	"net/http"

	"git.science.uu.nl/graphpolaris/go-common/microservice"
	"github.com/rs/zerolog/log"
)

/*
Handles incoming schema stat requests

	authService: auth.UseCase, the usecase for the authentication service
	produceService: produce.UseCase, the usecase for the producer service
	Return: http.Handler, returns an http handler
*/
func (s *SchemaService) Handler(w http.ResponseWriter, r *http.Request) {
	log.
		Trace().
		Str("schemaRequestHandler", "loading headers").
		Msg("processing schema stat request")

	// Publish a message into the query queue

	// Grab the databaseName and cached bool from the request
	var temp map[string]interface{}
	err := json.NewDecoder(r.Body).Decode(&temp)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	log.Trace().Str("schemaStatsRequestHandler", "processing body").Any("body", temp).Msg("processing schema stats request")

	databaseName, ok := temp["databaseName"].(string)
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	log.
		Trace().
		Str("schemaStatsRequestHandler - Databasename", databaseName).
		Msg("processing schema stats request")

	cached, ok := temp["cached"].(bool)
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Pass request to schema usecase
	sessionData, err := microservice.ContextToSessionData(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	body, err := json.Marshal(&temp) // json -> bytes
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	s.caching.RetrieveAsync(&body, fmt.Sprintf("%s-%s", sessionData.DatabaseName, sessionData.UserID), cached, sessionData, cached)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
