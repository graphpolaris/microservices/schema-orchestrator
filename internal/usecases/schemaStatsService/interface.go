package schemaStatsService

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"schema-orchestrator/internal/usecases/produce"

	"git.science.uu.nl/graphpolaris/go-common/caching"
	"git.science.uu.nl/graphpolaris/go-common/drivers/databaserpcdriver"
	"git.science.uu.nl/graphpolaris/go-common/structs"
	"git.science.uu.nl/graphpolaris/keyvaluestore"
	"github.com/rs/zerolog/log"
)

type SchemaService struct {
	producer *produce.ProduceService
	caching  *caching.Caching
}

func New(producer *produce.ProduceService, redisService keyvaluestore.Interface) *SchemaService {
	s := &SchemaService{
		producer: producer,
		caching:  caching.New("cached-schema-stats", redisService, "SCHEMA_RETRIEVAL_DURATION"),
	}

	s.caching.CacheMissCallback = s.CacheMissCallback
	s.caching.CacheHitCallback = s.CacheHitCallback
	return s
}

/*
CacheMissCallback publishes a schema retrieval request

	request: []byte, the schema retrieval request
	sessionID: string, the ID of the session
	userID: string, the ID of the client
	databaseName: string, the name of the database
*/
func (s *SchemaService) CacheMissCallback(request []byte, sessionData structs.SessionData) (data interface{}, err error) {
	headers := make(map[string]interface{})
	headers["sessionID"] = sessionData.SessionID
	headers["userID"] = sessionData.UserID

	// Send the message to the correct queue
	// Request the database type from the user management service
	databaseType, err := databaserpcdriver.GetDatabaseType(&sessionData.UserID, &sessionData.DatabaseName)
	if err != nil {
		log.Error().AnErr("Err", err).Msg("error retrieving database type No database type found")
		// 		log.Println("No valid database type found, defaulting to neo4j") // TODO
		// 		s.requestProducer.PublishMessage(&request, "neo4j-schema-request", &headers)
		return nil, err // TODO
	}

	switch *databaseType {
	case "arangodb":
		log.Trace().Msg("Publishing to arangodb stats queue")
		s.producer.RequestProducer.PublishMessageJsonHeaders(&request, "arangodb-schema-stats-request", &headers)
	case "neo4j":
		log.Trace().Msg("Publishing to neo4j stats queue")
		s.producer.RequestProducer.PublishMessageJsonHeaders(&request, "neo4j-schema-stats-request", &headers)
	default:
		log.Error().Msg("No valid database type found")
		return nil, errors.New("No valid database type found")
	}

	return nil, nil

}

/*
CacheHitCallback produces the cached schema

	schema: *[]byte, the schema result
	sessionID: *string, the ID of the session
*/
func (s *SchemaService) CacheHitCallback(data interface{}, sessionData structs.SessionData) error {
	// Use the sessionID to query the key value store to get the queue we need to send this message to
	clientQueueID, err := s.caching.KeyValueStore.Get(context.Background(), fmt.Sprintf("routing %s", sessionData.SessionID), keyvaluestore.String)
	if err != nil || clientQueueID == nil {
		return err
	}

	log.Trace().Str("sessionID", sessionData.SessionID).Str("userID", sessionData.UserID).Str("clientQueueID", clientQueueID.(string)).Msg("Found client queue for session")

	headers := make(map[string]interface{})
	headers["sessionID"] = sessionData.SessionID
	headers["userID"] = sessionData.UserID

	dataBytes, err := json.Marshal(data)
	if err != nil {
		return err
	}

	s.producer.ResultProducer.PublishMessageJsonHeaders(&dataBytes, clientQueueID.(string), &headers)
	return nil
}
